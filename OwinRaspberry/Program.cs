﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using OwinRaspberry;

[assembly: OwinStartup(typeof (Startup))]

namespace OwinRaspberry
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string baseUrl = @"http://*:5000";

            using (WebApp.Start<Startup>(baseUrl))
            {
                Console.WriteLine("Press Enter to quit...");
                Console.ReadKey();
            }
        }
    }
}